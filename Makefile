TARGETS=dumplog
INSTALLUSER=alarm@alarm
INSTALLPATH=racedash
#TCPREFIX = aarch64-linux-gnu-
TCPREFIX = 
CC      = $(TCPREFIX)gcc
CX      = $(TCPREFIX)g++
LD      = $(TCPREFIX)g++
AS      = $(TCPREFIX)as
CP      = $(TCPREFIX)objcopy
OD      = $(TCPREFIX)objdump
GDB     = $(TCPREFIX)gdb
GDBTUI  = $(TCPREFIX)gdbtui
NEMIVER = /usr/bin/nemiver
DDD     = /usr/bin/ddd

INCLUDES =	-iquote ./src
	

#LIBDIRS = -L ../odroid-c2/root/usr/lib
LIBS =  -lm -lpthread -lrt

MACHINE_OPTS=
CFLAGS  =  $(INCLUDES) $(DEFINES) -Wall -c -fdata-sections -ffunction-sections \
          -O3 -ggdb $(MACHINE_OPTS)

CXXFLAGS  = -std=c++11 -fno-exceptions $(CFLAGS)

CFLAGS += -DDEBUG

LFLAGS  = -Wl,--gc-sections -ggdb
#LFLAGS  = -ggdb -Wl,-Map $(TARGET).map

OBJDIR=obj
DEPDIR:=deps

DEPFLAGS = -MT $@ -MMD -MP -MF $(subst $(OBJDIR)/,,$(DEPDIR)/$@).Td
POSTCOMPILE = @mv -f $(DEPDIR)/$(subst $(OBJDIR)/,,$@).Td $(DEPDIR)/$(subst $(OBJDIR)/,,$@).d && touch $@

SRCFILES := src/racedashlog.cpp \
	    src/mlgbuilder.cpp \
            src/biquad.cpp \
            src/butterworth.cpp \
	    src/main.cpp

OUTOBJS    := $(addprefix $(OBJDIR)/,$(addsuffix .o,$(SRCFILES)))


all: dumplog


clean:
	@echo Cleaning...
	@rm -f $(TARGET)
	@rm -rf $(OBJDIR)
	@rm -rf $(DEPDIR)

dumplog: $(OUTOBJS)
	@echo Linking dumplog...
	@$(LD) $(LFLAGS) -o dumplog $(OUTOBJS) $(LIBDIRS) $(LIBS)


$(OUTOBJS): $(OBJDIR)/%.o : $(basename %) Makefile $(DEPDIR)/%.o.d
	@mkdir -p $(@D)
	@mkdir -p $(DEPDIR)/$(subst $(OBJDIR)/,,$(@D))
	@echo Compiling $<
	@$(CX) $(DEPFLAGS) $(CXXFLAGS) -o $@ $<
	$(POSTCOMPILE)


$(DEPDIR)/%.d: ;
.PRECIOUS: $(DEPDIR)/%.d

include $(patsubst %,$(DEPDIR)/%.o.d,$(SRCFILES))
