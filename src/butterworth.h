#ifndef BUTTERWORTH_H
#define BUTTERWORTH_H

#include"biquad.h"
#include <vector>


class ButterworthLP
{
   public:
    ButterworthLP(int order, double fc); // Order must be even
   ~ButterworthLP();

   inline double Process(double in)
   {
        double out = in;
        unsigned a;
        for (a = 0; a < mBiquads.size(); a++)
        {
            out = mBiquads[a].Process(out);
        }
        return out;
   }

  private:
    std::vector<Biquad> mBiquads;
};



#endif