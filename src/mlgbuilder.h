#ifndef MLGBUILDER_H
#define MLGBUILDER_H

#include "racedashlog.h"

void MLGBuilder(const char *fname, RacedashLog *source);

#endif // MLGBuilder_H