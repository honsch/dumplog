#ifndef ENDIAN_H
#define ENDIAN_H

#define LITTLE
//#define BIG

//-------------------------------------------------
template <typename T>
T HostToBig(T x)
{
#ifdef LITTLE
    union
    {
        T       x;
        uint8_t array[sizeof(T)];
    } swapper;
    swapper.x = x;
    unsigned f, b;
    f = 0;
    b = sizeof(T) - 1;
    while (f < b)
    {
        uint8_t temp = swapper.array[f];
        swapper.array[f] = swapper.array[b];
        swapper.array[b] = temp;
        ++f;
        --b;
    }
    return swapper.x;
#else
    return x;
#endif
}

//-------------------------------------------------
template <typename T>
T HostToLittle(T x)
{
#ifdef BIG
    union
    {
        T       x;
        uint8_t array[sizeof(T)];
    } swapper;
    swapper.x = x;
    unsigned f, b;
    f = 0;
    b = sizeof(T) - 1;
    while (f < b)
    {
        uint8_t temp = swapper.array[f];
        swapper.array[f] = swapper.array[b];
        swapper.array[b] = temp;
        ++f;
        --b;
    }
    return swapper.x;
#else
    return x;
#endif
}

//-------------------------------------------------
template <typename T>
T BigToHost(T x)
{
#ifdef LITTLE
    union
    {
        T       x;
        uint8_t array[sizeof(T)];
    } swapper;
    swapper.x = x;
    unsigned f, b;
    f = 0;
    b = sizeof(T) - 1;
    while (f < b)
    {
        uint8_t temp = swapper.array[f];
        swapper.array[f] = swapper.array[b];
        swapper.array[b] = temp;
        ++f;
        --b;
    }
    return swapper.x;
#else
    return x;
#endif
}

//-------------------------------------------------
template <typename T>
T LittleToHost(T x)
{
#ifdef BIG
    union
    {
        T       x;
        uint8_t array[sizeof(T)];
    } swapper;
    swapper.x = x;
    unsigned f, b;
    f = 0;
    b = sizeof(T) - 1;
    while (f < b)
    {
        uint8_t temp = swapper.array[f];
        swapper.array[f] = swapper.array[b];
        swapper.array[b] = temp;
        ++f;
        --b;
    }
    return swapper.x;
#else
    return x;
#endif
}


template double   HostToBig<double  >(double   x);
template float    HostToBig<float   >(float    x);
template uint64_t HostToBig<uint64_t>(uint64_t x);
template uint32_t HostToBig<uint32_t>(uint32_t x);
template uint16_t HostToBig<uint16_t>(uint16_t x);
template uint8_t  HostToBig<uint8_t >(uint8_t  x);
template int64_t  HostToBig<int64_t >(int64_t  x);
template int32_t  HostToBig<int32_t >(int32_t  x);
template int16_t  HostToBig<int16_t >(int16_t  x);
template int8_t   HostToBig<int8_t  >(int8_t   x);

template double   HostToLittle<double  >(double   x);
template float    HostToLittle<float   >(float    x);
template uint64_t HostToLittle<uint64_t>(uint64_t x);
template uint32_t HostToLittle<uint32_t>(uint32_t x);
template uint16_t HostToLittle<uint16_t>(uint16_t x);
template uint8_t  HostToLittle<uint8_t >(uint8_t  x);
template int64_t  HostToLittle<int64_t >(int64_t  x);
template int32_t  HostToLittle<int32_t >(int32_t  x);
template int16_t  HostToLittle<int16_t >(int16_t  x);
template int8_t   HostToLittle<int8_t  >(int8_t   x);

template double   BigToHost<double  >(double   x);
template float    BigToHost<float   >(float    x);
template uint64_t BigToHost<uint64_t>(uint64_t x);
template uint32_t BigToHost<uint32_t>(uint32_t x);
template uint16_t BigToHost<uint16_t>(uint16_t x);
template uint8_t  BigToHost<uint8_t >(uint8_t  x);
template int64_t  BigToHost<int64_t >(int64_t  x);
template int32_t  BigToHost<int32_t >(int32_t  x);
template int16_t  BigToHost<int16_t >(int16_t  x);
template int8_t   BigToHost<int8_t  >(int8_t   x);

template double   LittleToHost<double  >(double   x);
template float    LittleToHost<float   >(float    x);
template uint64_t LittleToHost<uint64_t>(uint64_t x);
template uint32_t LittleToHost<uint32_t>(uint32_t x);
template uint16_t LittleToHost<uint16_t>(uint16_t x);
template uint8_t  LittleToHost<uint8_t >(uint8_t  x);
template int64_t  LittleToHost<int64_t >(int64_t  x);
template int32_t  LittleToHost<int32_t >(int32_t  x);
template int16_t  LittleToHost<int16_t >(int16_t  x);
template int8_t   LittleToHost<int8_t  >(int8_t   x);


#endif //ENDIAN_H