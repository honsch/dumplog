#include <stdio.h>
#include <string.h>
#include <libgen.h>

#include "racedashlog.h"
#include "mlgbuilder.h"

//#define DUMP

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        printf("Usage: %s [logfile]\n", argv[0]);
        return 0;
    }

    RacedashLog *rdl = new RacedashLog(argv[1]);
    if (!rdl->IsValid())
    {
        printf("Could not read logfile %s, exiting\n", argv[1]);
        return -1;
    }

    printf("Loaded log %s\n", argv[1]);
    printf("This log has %d samples with %d channels and %d time indices.\n", rdl->TotalSamples(), rdl->NumItems(), rdl->TotalTimes());

#ifdef DUMP
    unsigned item;
    for (item = 0; item < rdl->NumItems(); item++)
    {
        printf("Item %2d: %s has %d samples of type %s.\n", item, rdl->ItemName(item), rdl->ItemSamples(item), rdl->ItemTypeName(item));
        unsigned a;
        for (a = 0; a < rdl->ItemSamples(item); a++)
        {
            const RacedashLog::Sample &s = rdl->ItemSample(item, a);
            printf("Sample %6d: %10.6f @ %12.5f\n", a, s.value, s.time);
        }
    }
#endif

    char outname[2048];
    strcpy(outname, basename(argv[1]));

    unsigned len = strlen(outname);
    if (0 == strcmp("rdl", &outname[len - 3]))
    {
        outname[len - 3] = 'm';
        outname[len - 2] = 'l';
        outname[len - 1] = 'g';
    }
    else
    {
        strcat(outname, ".mlg");
    }

    MLGBuilder(outname, rdl);

    return 0;
}