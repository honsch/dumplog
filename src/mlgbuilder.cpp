#include "mlgbuilder.h"
#include <algorithm>
#include "endian.h"

#include <string.h>
#include <stdio.h>
#include <stdint.h>

// for some insane reason all the data in the MLG formats is in BIG ENDIAN!
//--------------------------------
#pragma pack(push, 1)
struct LoggerFieldScalar
{
    uint8_t     type;           // 0=U08, 1=S08, 2=U16, 3=S16, 4=U32, 5=S32, 6=S64, 7=F32
    char        name[34];       // NULL terminated
    char        units[10];      // NULL terminated
    uint8_t     displayStyle;   // 0=Float, 1=Hex, 2=bits, 3=Date, 4=On/Off, 5=Yes/No, 6=High/Low, 7=Active/Inactive
    float       scale;          // displayValue = scale * (value + offset)
    float       offset;
    int8_t      digits;
};
#pragma pack(pop)


//--------------------------------
#pragma pack(push, 1)
struct LoggerFieldBit
{
    uint8_t     type;           // 10 = U08_BITFIELD, 11 = U16_BITFIELD, 12 = U32_BITFIELD
    char        name[34];       // NULL terminated
    char        units[10];      // NULL terminated
    uint8_t     displayStyle;   // 0=Float, 1=Hex, 2=bits, 3=Date, 4=On/Off, 5=Yes/No, 6=High/Low, 7=Active/Inactive
    uint32_t    namesIndex;
    uint8_t     numBits;
    uint8_t     _pad[3];
};
#pragma pack(pop)

//--------------------------------
#pragma pack(push, 1)
union LoggerField
{
    LoggerFieldBit    b;
    LoggerFieldScalar s;
};
#pragma pack(pop)

//--------------------------------
#pragma pack(push, 1)
struct LoggerFileHeader
{
    char        format[6];      // 'MLVLG\0'
    uint16_t    version;        // 0x0001
    uint32_t    timestamp;      // unix epoch time
    uint16_t    infoOffset;
    uint32_t    dataOffset;
    uint16_t    recordLength;
    uint16_t    numLoggerFields;
};
#pragma pack(pop)

//--------------------------------
#pragma pack(push, 1)
struct LoggerDataBlockHeader
{
    uint8_t  type; // 0x00=Data Block, 0x01=Marker Block
    uint8_t  counter;
    uint16_t timestamp;
};
#pragma pack(pop)


class MLGWriteBuffer
{
  public:
    MLGWriteBuffer(unsigned _maxSize)
    {
        maxSize = _maxSize;
        used = 0;
        buf = new unsigned char[maxSize];
    }

   ~MLGWriteBuffer()
   {
        delete[] buf;
   }

   void Clear(void)
   {
        used = 0;
        memset(buf, 0, maxSize);
   }

   void Write(FILE *outf)
   {
        fwrite(buf, 1, used, outf);
        WriteCRC(outf);
   }

   template <typename T> void Append(T x)
   {
        memcpy(&buf[used], &x, sizeof(x));
        used += sizeof(x);
   }

  private:
    unsigned char *buf;
    unsigned used;
    unsigned maxSize;

   void WriteCRC(FILE *outf)
   {
        unsigned char crc = 0;
        unsigned a;
        for (a = 0; a < used; a++) crc += buf[a];
        fwrite(&crc, 1, 1, outf);
   }
};


//----------------------------------------------------
void DumpHeader(const LoggerFileHeader &header)
{
    printf("Header info: %ld bytes", sizeof(header));
    printf("   version:         %d\n", BigToHost(header.version));
    printf("   timestamp:       %d\n", BigToHost(header.timestamp));
    printf("   infoOffset:      %d\n", BigToHost(header.infoOffset));
    printf("   dataOffset:      %d\n", BigToHost(header.dataOffset));
    printf("   recordLength:    %d\n", BigToHost(header.recordLength));
    printf("   numLoggerFields: %d\n", BigToHost(header.numLoggerFields));
    printf("\n");
}

//----------------------------------------------------
void DumpScalar(const LoggerFieldScalar &s, unsigned index)
{
    printf("Scalar Info %d:\n", index);
    printf("   Name:    %s\n", s.name);
    printf("   Units:   %s\n", s.units);
    printf("   Type:    %d\n", BigToHost(s.type));
    printf("   Display: %d\n", BigToHost(s.displayStyle));
    printf("   Scale:   %f\n", BigToHost(s.scale));
    printf("   Offset:  %f\n", BigToHost(s.offset));
    printf("   Digits:  %d\n", BigToHost(s.digits));
    printf("\n");
}

//----------------------------------------------------
//----------------------------------------------------
void MLGBuilder(const char *fname, RacedashLog *source)
{
    FILE *outf = fopen(fname, "wb");
    if (outf == NULL)
    {
        printf("Cannot open %s for writing\n", fname);
        return;
    }

    unsigned long totalSize = 0;
    std::vector<unsigned> validItems;
    validItems.reserve(source->NumItems());

    const unsigned sizeMap[8] = { 4, 4, 4, 4, 2, 2, 1, 1 };

    unsigned recordLength = 0;
    unsigned a;
    for (a = 0; a < source->NumItems(); a++)
    {
        if (source->ItemSamples(a) == 0) continue;
        validItems.push_back(a);
        totalSize += source->ItemSamples(a);
        recordLength += sizeMap[source->ItemType(a)];
    }

    std::vector<double> times;
    times.reserve(totalSize);

    // Gather all times
    for (a = 0; a < validItems.size(); a++)
    {
        unsigned index = validItems[a];
        unsigned b;
        for (b = 0; b < source->ItemSamples(index); b++)
        {
            if (source->ItemSample(index, b).time < 1000000.0)
            {
                printf("Invalid time on %s, Sample %d is at %f, dropping.\n", source->ItemName(index), b, source->ItemSample(index, b).time);
                continue;
            }
            times.push_back(source->ItemSample(index, b).time);
        }
    }

    std::stable_sort(times.begin(), times.end());
    auto last = std::unique(times.begin(), times.end());
    times.erase(last, times.end());
    printf("Reduced %ld times to %ld unique times\n", totalSize, times.size());

    printf("Log start time: %f, duration: %f\n", times[0], times[times.size() - 1] - times[0]);

    // Now we've got the list of times to get samples for
    // start by building the header

    LoggerFileHeader header;
    strcpy(header.format, "MLVLG");
    header.version         = HostToBig<uint16_t>(0x0001);
    header.timestamp       = HostToBig<uint32_t>(times[0]); // unix epoch time
    header.infoOffset      = HostToBig<uint16_t>(0);        // Offset from start of file where the info data is, zero is no info block
    header.dataOffset      = HostToBig<uint32_t>(sizeof(header) + (validItems.size() * sizeof(LoggerField))); // Offset from start of file where the data blocks start
    header.recordLength    = HostToBig<uint16_t>(recordLength);
    header.numLoggerFields = HostToBig<uint16_t>(validItems.size());

    //DumpHeader(header);

    fwrite(&header, sizeof(header), 1, outf);

    const uint8_t typeMap[8]    = { 7, 7, 4, 5, 2, 3, 0, 1 };
    const int8_t  decimalMap[8] = { 6, 4, 0, 0, 0, 0, 0, 0 };

    // Build and write the fields
    for (a = 0; a < validItems.size(); a++)
    {
        LoggerFieldScalar ls;
        memset(&ls, 0, sizeof(ls));

        unsigned index = validItems[a];
        uint8_t digits = decimalMap[source->ItemType(index)];
        if (source->ItemScale(index) < 1.00F) ++digits;
        if (source->ItemScale(index) < 0.10F) ++digits;
        if (source->ItemScale(index) < 0.01F) ++digits;

        strncpy(ls.name,  source->ItemName(index),  33);
        strncpy(ls.units, source->ItemUnits(index),  9);
        ls.type = HostToBig(typeMap[source->ItemType(index)]);
        ls.displayStyle = 0; // everything as floats
        ls.digits = HostToBig(digits);
        ls.scale  = HostToBig<float>(source->ItemScale(index));
        ls.offset = HostToBig<float>(0.0F);
        //DumpScalar(ls, a);
        fwrite(&ls, sizeof(ls), 1, outf);
    }

    // Write the samples!
    LoggerDataBlockHeader ldb;
    ldb.type    = 0;
    ldb.counter = 0;
    MLGWriteBuffer wb(header.recordLength);

    unsigned t;
    for (t = 0; t < times.size(); t++)
    {
        uint16_t ts = ((uint64_t) (100000.0 * times[t]) & 0xFFFF);
        ldb.timestamp = HostToBig<uint16_t>(ts);

        fwrite(&ldb, sizeof(ldb), 1, outf);
        ++ldb.counter;

        wb.Clear();

        for (a = 0; a < validItems.size(); a++)
        {
            double value = 0.0;
            unsigned index = validItems[a];

            int si = source->ItemIndexAtTime(index, times[t]);
            if (si >= 0) value = source->ItemSample(index, si).value;
            value /= source->ItemScale(index);

            switch (source->ItemType(index))
            {
                case RacedashLog::Double:
                case RacedashLog::Float:
                    wb.Append(HostToBig<float>(value));
                    break;

                case RacedashLog::U32:
                case RacedashLog::S32:
                    wb.Append(HostToBig<uint32_t>(value));
                    break;

                case RacedashLog::U16:
                case RacedashLog::S16:
                    wb.Append(HostToBig<uint16_t>(value));
                    break;

                case RacedashLog::U8:
                case RacedashLog::S8:
                    wb.Append(HostToBig<uint8_t>(value));
                    break;
            }
        }
        wb.Write(outf);
    }

    fclose(outf);
}
