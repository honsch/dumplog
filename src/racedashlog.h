#ifndef RACEDASHLOG_H
#define RACEDASHLOG_H

#include <stdint.h>
#include <stdio.h>
#include <vector>

//--------------------------------------------
class RacedashLog
{
  public:
    RacedashLog(const char *fname);
   ~RacedashLog();

    struct Sample
    {
        double time;
        double value;
    };

    enum LogVariableType { Double, Float, U32, S32, U16, S16, U8, S8 };

    bool            IsValid(void) const { return mTotalSamples > 0; }

    unsigned        NumItems(void)     const { return mLogItems.size(); }
    unsigned        TotalSamples(void) const { return mTotalSamples; }
    unsigned        TotalTimes(void)   const { return mTotalTimeIndices; }

    const char     *ItemName(unsigned itemIndex)  const { return mLogItems[itemIndex].name; }
    const char     *ItemUnits(unsigned itemIndex) const { return mLogItems[itemIndex].units; }
    const char     *ItemTypeName(unsigned itemIndex)  const { return LogTypeNames[mLogItems[itemIndex].type]; }
    LogVariableType ItemType(unsigned itemIndex)  const { return (LogVariableType) mLogItems[itemIndex].type; }
    float           ItemScale(unsigned itemIndex)  const { return mLogItems[itemIndex].scale; }
    unsigned        ItemSamples(unsigned itemIndex) const { return mLogItems[itemIndex].samples.size(); }
    const Sample   &ItemSample(unsigned itemIndex, unsigned sampleIndex) const { return mLogItems[itemIndex].samples[sampleIndex]; }

    // This returns the index of the sample at or latest before time specified
    // returns -1 on empty samples
    int             ItemIndexAtTime(unsigned itemIndex, double time);

    bool            ItemDisplay(unsigned itemIndex) const { return mLogItems[itemIndex].display; }
    void            SetItemDisplay(unsigned itemIndex, bool en) { mLogItems[itemIndex].display = en; }

    int             FindItemByName(const char *name);

  private:

    bool LoadLogTimeIndex(FILE *inf);

    void PostProcess(void);

    typedef uint32_t DataLoggerID;
    static const char *LogTypeNames[8];

    //----------------------------------------------------
    struct LogHeaderVariable
    {
        char         name[64];
        char         units[64];
        double       scale;
        uint32_t     type;
        DataLoggerID id;
    };

    //----------------------------------------------------
    struct LogHeader
    {
        uint32_t magic;
        uint32_t version;
        uint32_t numVariables;
        uint32_t _pad;
        LogHeaderVariable _variables[0];
    };

    //----------------------------------------------------
    struct LogTimeIndex
    {
        LogTimeIndex(uint64_t t = 0) : time(t) {}
        uint32_t magic;
        uint32_t numVariables;
        uint64_t time;
        uint8_t  data[0];
    };

    // Internal use
    struct LogItem
    {
        char        *name;
        char        *units;
        bool         display;
        double       scale;
        uint32_t     type;
        DataLoggerID id;
        std::vector<Sample> samples;
        unsigned     lastFoundIndex;
    };

    int FindLogItemByID(DataLoggerID id);
    std::vector<LogItem> mLogItems;
    unsigned mTotalSamples;
    unsigned mTotalTimeIndices;
};

#endif // RACEDASHLOG_H
